/*
7. Pokerkocka A jatekhoz 5 kockat hasznalnak. Minden jatekos haromszor dob. A cel a legjobb pokerfigura elerese. Elso dobaskor a jatekos mind az ot kockaval dob.
Ezutan a kockak kozul tetszoleges szamut felretehet egy esetleges pokerfigurahoz, s a maradek kockakkal ujra dob. 
A masodik dobasbol ismet felretehet tetszoleges szamu kockat, majd harmadszor is ujra dob.
A jatekos barmikor megallhat, nem szukseges mindharom dobast vegrehajtania. A felretett kockakat nem lehet ujra hasznalni. 
A legnagyobb erteku pokerfigura nyer. Ha ket jatekos azonos erteket er el, ujabb jatekot kell jatszaniuk.
Irjunk egy olyan programot, mely pokerkockat jatszik a felhasznaloval. A pokerfigurak ertekeknek megfeleloen felulrol lefele:  Ot egyforma (peldaul: 4-4-4-4-4)  Poker (peldaul: 1-1-1-1-4)  
Full (peldaul: 1-1-1-2-2) Magas sor (peldaul: 6-5-4-3-2)  Alacsony sor (peldaul: 5-4-3-2-1)  Drill (peldaul: 1-1-1-2-3)  Ket par (peldaul: 1-1-2-2-3) 
Egy par (peldaul: 1-1-2-3-4)  Semmi (peldaul: 1-2-3-4-6)


*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

typedef struct/* Jatekos stuktura*/
{
	int kocka[5];
	int locked[5];
	char nev[50];
	int ertek;

}jatekos;


void rendezo(int *tomb, int *locked);
void generator(int *kocka, int *locked);
int ertek(int *kocka);
void locker(int *kocka, int *locked);
char *erteknev(int value);
void kiiras(int *kocka, int *locked);
void jatek(int *kocka, int *locked, char *nev);
void duplatic(jatekos *jatekosok, int n);
int dup_check(jatekos *jatekosok, int n);
void gyoztes(jatekos *jatekosok, int n);
/*Fuggevenyek elore deklaralasa hogy barhol fel lehessen oket haszalni*/

int main()
{
	int n=0;
	int i = 0;
	printf("Adja meg a jatekosok szamat!\n");
	scanf_s("%d", &n);
	jatekos* jatekosok = (jatekos*)calloc(n, sizeof(jatekos));/* A bekert jatekos szam alapjan dinamikusan letrehozunk egy struktura tombot*/
	for ( i = 0; i < n; i++)
	{
		printf("Adja meg a(z) %d jatekosok nevet!\n",i+1);/* Bekerjuk a neveket es hozzarendeljuk a valtozot */
		scanf_s("%s", jatekosok[i].nev, 50);
	}

	for ( i = 0; i < n; i++)
	{
		memset(jatekosok[i].kocka, 0, 5*sizeof(int));/* kinulazzuk a tomb kocka ertekeit */
		memset(jatekosok[i].locked, 0, 5*sizeof(int));/* kinulazzuk a tomb locked ertekeit*/
		jatek(jatekosok[i].kocka, jatekosok[i].locked, jatekosok[i].nev);/* meghivjuk a jatek fuggvenyt */
		jatekosok[i].ertek = ertek(jatekosok[i].kocka);/* jatek vegen az eredmenyt eltaroljuk */
	}

	while (dup_check(jatekosok,n) == 1)/* meghivjuk azt a fuggvenyt amivel megvizsgaljuk hogy van-e ket egyforma erteku dobas */
	{
		duplatic(jatekosok, n);/* ha van akkor meghivjuk ezt a fuggvenyt amivel uj jatekot kerunk a duplas jatekosoktol */
	}
	gyoztes(jatekosok,  n);/* kiirjuk a gyoztesek neveit */
	return 0;
}

void generator(int *kocka, int *locked)/* Random szammokkat generalunk ezzel szimulaljuk a kockat, csak azokat az ertekeket generaljuk ujra ahol nincsen befagyasztva a kocka */
{
	srand((unsigned)time(NULL));
	int i;
	for (i = 0; i < 5; i++) 
	{
		if ( *(locked +i) == 0)
		{
			*(kocka + i) = rand() % 6 + 1;
		}
	}

}

/* Megkerdezzuk a jatekost hogy milyen kockakat szeretne megtartani majd amit beir szam megvizsgaljuk hogy le van-e mar fagyasztva vagy egyatalan letezik, ha letezik a kocka es nem volt meg
felhasznalva akkor befagyasszuk igy a kesobbi generalas soran a generator nem fogja figyelembe venni es az erteke nem fog valtozni, addig kerjuk be a szamokat amig a jatekos nem ir be nullat*/
void locker(int *kocka, int *locked)
{
	int i,szam;
	printf("\nMelyik kockakat szeretned megtartani? (Egyesevel add meg! Irj 0 ha nem szeretnel tobbet megtartani!)\n");
	scanf_s("%d", &szam);
	while (szam != 0)
	{

		for (i = 0; i < 5; i++)
		{
			if ((*(kocka + i) == szam) && (*(locked + i) == 0))
			{
				break;
			}
		}
		if (i != 5)
		{
			*(locked + i) = 1;
			printf("\nKocka felrerakva!\n");
		}
		else
		{
			printf("\nA kocka mar fellre van teve vagy nem letezik!\n");
		}
		scanf_s("%d", &szam);
	}

}


/*3 korig fog a ciklus szimulalni, 1. korben general 5 kockahoz 1-zol 6-ig szamokat, utana rendezzuk a kockakat hogy a jatekos a programhoz egyszerubben lehessen kezelni, majd kiirjuk az erteket
utana meghivjuk a lockert amivel a jatekos lefagyaszthatja azokat a kockakat amiket meg szeretne tartani, majd a ciklus végén kiirjuk a vegso eredmenyt.*/
void jatek(int *kocka,int *locked, char *nev)
{
	int i;
	for (i = 0; i < 3; i++)
	{
		printf("\n%s kovetkezik %d. kor\n", nev,i + 1);
		generator(kocka,locked);
		rendezo(kocka,locked);
		kiiras(kocka,locked);
		locker(kocka, locked);
	}
	printf("Vegso eredmeny: ");
	kiiras(kocka, locked);
}

/* Megvizsgalja a az adott kockakbol milyen eredmenyt lehet kihozni, fontos hogy a kockak sorba legyenek rendezve mivel igy egyszerubb vizsgalni es a fuggveny is csak ugy tud mukodni */
int ertek(int *kocka)
{
	int i;
	int szam[6] = { 0, 0, 0, 0, 0 };
	int full[2] = { 0,0 };
	int par=0;

	/* Nem magat a kockakat fogjuk vizsglani hanem egy tombhoz hozzarendeljuk hogy melyik kockabol mennyi van */
	for (i = 0; i < 5; i++)
	{
		szam[(*(kocka + i))-1]++;
	}

	for ( i = 0; i < 6; i++) /* Megvizsgaljuk hogy van e olyan tomben olyan ertek ami 5 vagyis 5db egyforma kocka van ha igen annak az erteke 9 lesz, mivel 5 kockanal tobb nem lehet ezert kozben vizsgalhatjuk a 4 is  */
	{
		if (szam[i] == 5)
		{
			return 9;
		}
		if (szam[i] == 4)
		{
			return 8;
		}
	}

	for (i = 0; i < 6; i++)/* Megnezzuk hogy van a tomben olyan ertek amikor a kockakbol 3 es 2 egyforma van*/
	{
		if (szam[i] == 3)
		{
			full[0] = 1;
		}
		if (szam[i] == 2)
		{
			full[1] = 1;
		}
		if (full[0] == 1 && full[1] == 1)/* csak akkor lehet full ha 3 es 2 egyforma van*/
		{
			return 7;
		}
	}
	if (szam[5] == 1)/* nagy sornal megvizsgaljuk az utolso kockat aminek 6-nak kell lennie, ha van belole 1 darab akkor vizsgalhatjuk tovabb hogy meg van hozza a maradek ertek is 
					 de ha nincs vagy tobb 6-os van akkor folosleges megvizsgalni mivel mashogy nem johet ki a sor*/
	{
		for (i = 5; i > 0; i--)
		{
			if (szam[i] != 1)
			{
				break;
			}
		}
		if (i == 0)
		{
			return 6;
		}
	}

	if (szam[4] == 1)/* ugyanaz mint a nagy sornal de itt a 5-os erteket vizsgaljuk meg hogy van-e 1 darab es utana hogy meg van a tobbi szukseges ertek is*/
	{
		for (i = 4; i >= 0; i--)
		{
			if (szam[i] != 1)
			{
				break;
			}
		}
		if (i == -1)
		{
			return 5;
		}
	}

	for (i = 0; i < 6; i++)/* drillhez megnezzuk hogy van 3 egyforma kocka*/
	{
		if (szam[i] == 3)
		{
			return 4;
		}
	}

	for (i = 0; i < 6; i++)/* megnezzuk hogy van e parban 2 egyforma ertek, pld 3-3,2-2*/
	{
		if (szam[i] == 2)
		{
			par++;
		}
		if (par == 2)
		{
			return 3;
		}
	}

	for (i = 0; i < 6; i++)/* part keresunk */
	{
		if (szam[i] == 2)
		{
			return 2;
		}
	}

	return 1;
}

void rendezo(int *tomb,int *locked)/* buborek rendezo, sorba rakja a kockakat*/
{
	int i, j, temp, ttemp;

	for (j = 5 - 1; j > 0; j--) 
	{
		for (i = 0; i < j; i++)
		{
			if( *(tomb + i) > *(tomb + i+1))
			{
				temp = *(tomb + i);
				*(tomb + i) = *(tomb + i+1);
				*(tomb + i+1) = temp;

				ttemp = *(locked + i);
				*(locked + i) = *(locked + i+1);
				*(locked + i+1) = ttemp;
			}
			
		}
	}

}

char *erteknev(int value)/* visszaadja az adott ertekhez tartozo ertek nevet*/
{
	switch (value)
	{
		case 1:
			return  "Semmi";
			break;
		case 2:
			return  "Egy par";
			break;
		case 3:
			return  "Ket par";
			break;
		case 4:
			return  "Drill";
			break;
		case 5:
			return  "Alacsony sor";
			break;
		case 6:
			return  "Magas sor";
			break;
		case 7:
			return  "Full";
			break;
		case 8:
			return  "Poker";
			break;
		case 9:
			return  "Ot egyforma";
			break;
	default:
		break;
	}
	return "";
}

void kiiras(int *kocka, int *locked)/* kiirjuk a kockak erteketit *szam* formatumu a fagyasztott kockak */
{ 
	int i;
	for (i = 0; i < 5; i++)
	{
		if ( *(locked+i) == 1)
		{
			printf("*%d* \t", *(kocka + i));
		}
		else
		{
			printf("%d \t", *(kocka + i));
		}
		
	}
	printf("%s", erteknev(ertek(kocka)));

}

void duplatic(jatekos *jatekosok, int n)/* Megkeresi az azonos erteku jatekosokat majd azokkal megegy mecset lejatszik, fontos hogy akiknek biztos helyezesuk van azok nem valtoznak csak 
										az azonos erteku helyezetek valtoztathatnak helyezest*/
{
	int i,j,k=0,du=0;
	int* dup = (int*)malloc(n * sizeof(int)); /* letrehozunk egy dinamikus tombot */
	memset(dup, -1, n * sizeof(int)); /* feltoljuk a tombot -1 */

	for ( i = 0; i < n; i++)
	{
		for ( j = i+1; j < n; j++)
		{
			if (jatekosok[i].ertek == jatekosok[j].ertek)
			{
				dup[k] = j;
				k++;
				du = 1;
			}
		}
		if (du == 1)
		{
			dup[k] = i;
			k++;
		}
		du = 0;
		jatekosok[i].ertek *= 10; /* minden jatekosnak az erteket megszorozzuk tizzel hogy a kovetkezonek jatszo jatekosok ne befolyasoljak a biztos helyezettek helyet */
	}

	for ( i = 0; i < n; i++)/* ujra lejatszuk a jatekoz az azonos erteku jatekosokkal*/
	{
		if (dup[i] != -1)
		{
			printf("\n%s azonos pontszam miatt ujra \n", jatekosok[dup[i]].nev);
			memset(jatekosok[dup[i]].locked, 0, 5*sizeof(int));
			memset(jatekosok[dup[i]].kocka, 0, 5*sizeof(int));
			jatek(jatekosok[dup[i]].kocka, jatekosok[dup[i]].locked, jatekosok[dup[i]].nev);
			jatekosok[dup[i]].ertek += ertek(jatekosok[dup[i]].kocka);/* a felszorzott ertekethez hozaadjuk az ujat igy csak az azonos erteku jatekosoknak valtozik a helyezesuk*/
		}
	}

	free(dup);
}

int dup_check(jatekos *jatekosok, int n)/* megvizsgalja a jatekosokat hogy van-e ugyanolyan pontszamu jatekos ha van akkor visszater 1-es ertekkel*/
{
	int i, j;
	for (i = 0; i < n; i++)
	{
		for (j = i + 1; j < n ; j++)
		{
			if (jatekosok[i].ertek == jatekosok[j].ertek)
			{
				return 1;
			}
		}
	}
	return 0;
}

void gyoztes(jatekos *jatekosok, int n)/* kiirja a gyoztes jatekosokat helyezettel.*/
{
	int i,j;
	jatekos temp;
	for (j = n - 1; j > 0; j--)
	{
		for (i = 0; i < j; i++)
		{

			if (jatekosok[i].ertek < jatekosok[i+1].ertek)
			{
				temp = jatekosok[i];
				jatekosok[i] = jatekosok[i+1];
				jatekosok[i+1] = temp;
			}
		}
	}
	for ( i = 0; i < n; i++)
	{
		printf("\n%d. helyezett --> %s\n", i + 1, jatekosok[i].nev);
	}
}